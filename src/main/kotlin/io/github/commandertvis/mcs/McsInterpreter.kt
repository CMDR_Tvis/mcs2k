package io.github.commandertvis.mcs

import io.github.commandertvis.jni.JNI

public class McsInterpreter(workersQuantity: Int) : AutoCloseable {
    private var ptr: Long = 0L

    init {
        initialize(workersQuantity)
    }

    public data class Result<T> internal constructor(val errors: List<String>?, val value: T?) {
        inline fun ifValue(action: (T) -> Unit): Result<T> = apply { value?.let(action) }
        inline fun ifErrors(action: (List<String>) -> Unit): Result<T> = apply { errors?.let(action) }
        fun hasErrors(): Boolean = errors != null
        fun hasValue(): Boolean = value != null

        internal companion object {
            @JvmStatic
            public fun <T> ofErrors(errors: List<String>): Result<T> = Result(errors = errors, value = null)

            @JvmStatic
            public fun <T> ofValue(value: T): Result<T> = Result(errors = null, value = value)
        }
    }

    private external fun initialize(workersQuantity: Int)

    public external fun histogram(
        expression: String, nValues: Long, xMin: Double, xMax: Double, histogramMin: Double,
        histogramMax: Double, nBins: Long
    ): Result<List<Double>>?

    public external override fun close()

    public companion object {
        init {
            JNI.loadLibraryFromJar<McsInterpreter>(path = "/libmcs2k.so")
        }

        @JvmStatic
        public external fun plot(
            expression: String,
            fromInclusive: Double,
            toExclusive: Double,
            nPoints: Long
        ): Result<Map<Double, Double>>?

        @JvmStatic
        public external fun evaluate(expression: String, x: Double): Result<Double>?
    }
}
