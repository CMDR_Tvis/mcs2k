#include "io_github_commandertvis_mcs_McsInterpreter.h"
#include <McsInterpreter.hpp>

inline auto ArrayListFromStringVector(JNIEnv *env,
                                      std::vector<std::string> *source)
    -> jobject {
  auto arrayListClass = env->FindClass("java/util/ArrayList");
  auto arrayListInit = env->GetMethodID(arrayListClass, "<init>", "(I)V");

  auto arrayListAdd =
      env->GetMethodID(arrayListClass, "add", "(Ljava/lang/Object;)Z");

  auto arrayList = env->NewObject(arrayListClass, arrayListInit,
                                  static_cast<jint>(source->size()));

  std::for_each(source->begin(), source->end(),
                [env, arrayList, arrayListAdd](std::string &i) {
                  env->CallBooleanMethod(arrayList, arrayListAdd,
                                         env->NewStringUTF(i.c_str()));
                });

  return arrayList;
}

inline auto ArrayListFromLongDoubleVector(JNIEnv *env,
                                          std::vector<long double> *source)
    -> jobject {
  auto arrayListClass = env->FindClass("java/util/ArrayList");
  auto arrayListInit = env->GetMethodID(arrayListClass, "<init>", "(I)V");

  auto arrayListAdd =
      env->GetMethodID(arrayListClass, "add", "(Ljava/lang/Object;)Z");

  auto arrayList = env->NewObject(arrayListClass, arrayListInit,
                                  static_cast<jint>(source->size()));

  const auto javaDoubleClass = env->FindClass("java/lang/Double");

  const auto javaDoubleValueOf = env->GetStaticMethodID(
      javaDoubleClass, "valueOf", "(D)Ljava/lang/Double;");

  std::for_each(source->begin(), source->end(),
                [env, arrayList, arrayListAdd, javaDoubleClass,
                 javaDoubleValueOf](long double &i) {
                  env->CallBooleanMethod(arrayList, arrayListAdd,
                                         env->CallStaticObjectMethod(
                                             javaDoubleClass, javaDoubleValueOf,
                                             static_cast<jdouble>(i)));
                });

  return arrayList;
}

inline auto LinkedHashMapFromLongDoubleLongDoubleMap(
    JNIEnv *env, std::map<long double, long double> *source) -> jobject {
  const auto linkedHashMapClass = env->FindClass("java/util/LinkedHashMap");

  const auto linkedHashMapInit =
      env->GetMethodID(linkedHashMapClass, "<init>", "(I)V");

  const auto hashMapClass = env->FindClass("java/util/HashMap");

  const auto hashMapPut = env->GetMethodID(
      hashMapClass, "put",
      "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");

  auto linkedHashMap = env->NewObject(linkedHashMapClass, linkedHashMapInit,
                                      static_cast<jint>(source->size()));

  const auto javaDoubleClass = env->FindClass("java/lang/Double");

  const auto javaDoubleValueOf = env->GetStaticMethodID(
      javaDoubleClass, "valueOf", "(D)Ljava/lang/Double;");

  std::for_each(
      source->begin(), source->end(),
      [env, linkedHashMap, hashMapPut, javaDoubleClass,
       javaDoubleValueOf](std::pair<long double, long double> p) {
        env->CallObjectMethod(
            linkedHashMap, hashMapPut,
            env->CallStaticObjectMethod(javaDoubleClass, javaDoubleValueOf,
                                        static_cast<jdouble>(p.first)),
            env->CallStaticObjectMethod(javaDoubleClass, javaDoubleValueOf,
                                        static_cast<jdouble>(p.second)));
      });

  return linkedHashMap;
}

inline auto JavaResultFromConstLongDoubleResult(
    JNIEnv *env, mcs::McsInterpreter::Result<const long double> *result)
    -> jobject {
  const auto javaResultClass =
      env->FindClass("io/github/commandertvis/mcs/McsInterpreter$Result");

  const auto javaResultOfErrors = env->GetStaticMethodID(
      javaResultClass, "ofErrors",
      "(Ljava/util/List;)Lio/github/commandertvis/mcs/McsInterpreter$Result;");

  const auto javaResultOfValue =
      env->GetStaticMethodID(javaResultClass, "ofValue",
                             "(Ljava/lang/Object;)Lio/github/commandertvis/mcs/"
                             "McsInterpreter$Result;");

  const auto javaDoubleClass = env->FindClass("java/lang/Double");

  const auto javaDoubleValueOf = env->GetStaticMethodID(
      javaDoubleClass, "valueOf", "(D)Ljava/lang/Double;");

  if (result->hasErrors())
    return env->CallStaticObjectMethod(
        javaResultClass, javaResultOfErrors,
        ArrayListFromStringVector(env, result->errorsOrNull()));

  return env->CallStaticObjectMethod(
      javaResultClass, javaResultOfValue,
      env->CallStaticObjectMethod(
          javaDoubleClass, javaDoubleValueOf,
          static_cast<jdouble>(*result->valueOrNull())));
}

inline auto JavaResultFromLongDoubleLongDoubleMapResult(
    JNIEnv *env,
    mcs::McsInterpreter::Result<std::map<long double, long double>> *result)
    -> jobject {
  const auto javaResultClass =
      env->FindClass("io/github/commandertvis/mcs/McsInterpreter$Result");

  const auto javaResultOfErrors = env->GetStaticMethodID(
      javaResultClass, "ofErrors",
      "(Ljava/util/List;)Lio/github/commandertvis/mcs/McsInterpreter$Result;");

  const auto javaResultOfValue =
      env->GetStaticMethodID(javaResultClass, "ofValue",
                             "(Ljava/lang/Object;)Lio/github/commandertvis/mcs/"
                             "McsInterpreter$Result;");

  if (result->hasErrors())
    return env->CallStaticObjectMethod(
        javaResultClass, javaResultOfErrors,
        ArrayListFromStringVector(env, result->errorsOrNull()));

  return env->CallStaticObjectMethod(
      javaResultClass, javaResultOfValue,
      LinkedHashMapFromLongDoubleLongDoubleMap(env, result->valueOrNull()));
}

inline auto JavaResultFromLongDoubleVectorResult(
    JNIEnv *env, mcs::McsInterpreter::Result<std::vector<long double>> *result)
    -> jobject {
  const auto javaResultClass =
      env->FindClass("io/github/commandertvis/mcs/McsInterpreter$Result");

  const auto javaResultOfErrors = env->GetStaticMethodID(
      javaResultClass, "ofErrors",
      "(Ljava/util/List;)Lio/github/commandertvis/mcs/McsInterpreter$Result;");

  const auto javaResultOfValue =
      env->GetStaticMethodID(javaResultClass, "ofValue",
                             "(Ljava/lang/Object;)Lio/github/commandertvis/mcs/"
                             "McsInterpreter$Result;");

  if (result->hasErrors())
    return env->CallStaticObjectMethod(
        javaResultClass, javaResultOfErrors,
        ArrayListFromStringVector(env, result->errorsOrNull()));

  return env->CallStaticObjectMethod(
      javaResultClass, javaResultOfValue,
      ArrayListFromLongDoubleVector(env, result->valueOrNull()));
}

auto Java_io_github_commandertvis_mcs_McsInterpreter_initialize(
    JNIEnv *env, jobject _this, jint workersQuantity) -> void {
  try {
    const auto mcsInterpreterClass = env->GetObjectClass(_this);

    const auto mcsInterpreterPtr =
        env->GetFieldID(mcsInterpreterClass, "ptr", "J");

    env->SetLongField(_this, mcsInterpreterPtr,
                      reinterpret_cast<jlong>(new mcs::McsInterpreter(
                          static_cast<unsigned short int>(workersQuantity))));
  } catch (...) {
  }
}

auto Java_io_github_commandertvis_mcs_McsInterpreter_histogram(
    JNIEnv *env, jobject _this, jstring expression, jlong nValues, jdouble xMin,
    jdouble xMax, jdouble histogramMin, jdouble histogramMax, jlong nBins)
    -> jobject {
  try {
    auto result =
        (reinterpret_cast<mcs::McsInterpreter *>(env->GetLongField(
             _this, env->GetFieldID(env->GetObjectClass(_this), "ptr", "J"))))
            ->histogram(
                std::string(env->GetStringUTFChars(expression, JNI_FALSE)),
                static_cast<unsigned long long int>(nValues),
                static_cast<long double>(xMin), static_cast<long double>(xMax),
                static_cast<long double>(histogramMin),
                static_cast<long double>(histogramMax),
                static_cast<unsigned long long int>(nBins));

    return JavaResultFromLongDoubleVectorResult(env, result);
  } catch (...) {
  }

  return nullptr;
}

auto Java_io_github_commandertvis_mcs_McsInterpreter_close(JNIEnv *env,
                                                           jobject _this)
    -> void {
  try {
    const auto mcsInterpreterClass = env->GetObjectClass(_this);

    const auto mcsInterpreterPtr =
        env->GetFieldID(mcsInterpreterClass, "ptr", "J");

    delete reinterpret_cast<mcs::McsInterpreter *>(
        env->GetLongField(_this, mcsInterpreterPtr));
  } catch (...) {
  }
}

auto Java_io_github_commandertvis_mcs_McsInterpreter_plot(
    JNIEnv *env, jclass, jstring expression, jdouble fromInclusive,
    jdouble toExclusive, jlong nPoints) -> jobject {
  try {
    const auto expressionString = env->GetStringUTFChars(expression, JNI_FALSE);

    const auto result = mcs::McsInterpreter::plot(
        expressionString, static_cast<long double>(fromInclusive),
        static_cast<long double>(toExclusive),
        static_cast<unsigned long long int>(nPoints));

    return JavaResultFromLongDoubleLongDoubleMapResult(env, result);
  } catch (...) {
  }

  return nullptr;
}

auto Java_io_github_commandertvis_mcs_McsInterpreter_evaluate(
    JNIEnv *env, jclass, jstring expression, jdouble x) -> jobject {
  try {
    const auto expressionString = env->GetStringUTFChars(expression, JNI_FALSE);

    const auto result = mcs::McsInterpreter::evaluate(
        expressionString, static_cast<long double>(x));

    return JavaResultFromConstLongDoubleResult(env, result);
  } catch (...) {
  }

  return nullptr;
}
