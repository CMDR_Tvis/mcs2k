plugins { `maven-publish`; kotlin("jvm") }
val jniVersion: String by project
val junitJupiterVersion: String by project
val kotlinApiVersion: String by project
val kotlinJvmTarget: String by project
val kotlinLanguageVersion: String by project
val kotlinVersion: String by project
val mcs2kVersion: String by project
group = "io.github.commandertvis"
version = mcs2kVersion

repositories {
    jcenter()
    maven(url = "https://gitlab.com/api/v4/projects/11000558/packages/maven")
}

dependencies {
    api("io.github.commandertvis:jni:$jniVersion")
    implementation(kotlin(module = "stdlib-jdk8", version = kotlinVersion))
}

tasks {
    compileKotlin {
        kotlinOptions {
            apiVersion = kotlinApiVersion
            jvmTarget = kotlinJvmTarget
            languageVersion = kotlinLanguageVersion
        }
    }

    val buildDir = file(path = "cmake-build-release/")

    val execCmake = task<Exec>(name = "execCmake") {
        if (!buildDir.exists())
            buildDir.mkdirs()

        workingDir = buildDir

        commandLine("cmake", "-DCMAKE_BUILD_TYPE=Release", "..")
    }

    val execMake = task<Exec>(name = "execMake") {
        if (!buildDir.exists())
            buildDir.mkdirs()

        workingDir = buildDir
        commandLine("make")
    }

    jar {
        dependsOn(execCmake, execMake)
        from(buildDir.toPath()) { include("libmcs2k.so") }
    }

    val sourcesJar = task<Jar>(name = "sourcesJar") {
        from(sourceSets.main.get().allSource)
        archiveClassifier.set("sources")
    }

    publish.get().dependsOn(assemble)

    publishing {
        publications {
            create<MavenPublication>("mavenJava") {
                from(project.components["java"])
                artifacts { artifact(sourcesJar) }

                pom {
                    operator fun <T> Property<T>.invoke(value: T) = set(value)

                    operator fun <T> SetProperty<T>.invoke(vararg value: T) = set(value.toList())

                    packaging = "jar"
                    name(project.name)
                    description(project.description)
                    url("https://gitlab.com/CMDR_Tvis/${project.name}")
                    inceptionYear("2019")

                    licenses {
                        license {
                            comments("Open-source license")
                            distribution("repo")
                            name("MIT License")
                            url("https://gitlab.com/CMDR_Tvis/${project.name}/blob/master/LICENSE")
                        }
                    }

                    developers {
                        developer {
                            email("postovalovya@gmail.com")
                            id("CMDR_Tvis")
                            name("Commander Tvis")
                            roles("architect", "developer")
                            timezone("7")
                            url("https://commandertvis.github.io")
                        }
                    }

                    scm {
                        connection("scm:git:git@gitlab.com:CMDR_Tvis/${project.name}.git")
                        developerConnection("scm:git:git@gitlab.com:CMDR_Tvis/${project.name}.git")
                        url("git@gitlab.com:CMDR_Tvis/${project.name}.git")
                    }
                }
            }

            repositories {
                maven(url = "https://gitlab.com/api/v4/projects/17803458/packages/maven") {
                    credentials(HttpHeaderCredentials::class) {
                        name = "Job-Token"
                        value = System.getenv("CI_JOB_TOKEN")
                    }

                    authentication { register(name = "header", type = HttpHeaderAuthentication::class) }
                }
            }
        }
    }
}
