cmake_minimum_required(VERSION 3.15.3)
include(ExternalProject)

if (NOT DEFINED MCS_TAG)
    set(MCS_TAG master)
endif ()

set(MCS_ROOT ${CMAKE_SOURCE_DIR}/external/monte-carlo-script)
set(MCS_GIT_REPOSITORY https://gitlab.com/CMDR_Tvis/monte-carlo-script.git)

ExternalProject_Add(
        mcs
        PREFIX mcs
        GIT_REPOSITORY ${MCS_GIT_REPOSITORY}
        GIT_TAG ${MCS_TAG}
        DOWNLOAD_DIR ${CMAKE_SOURCE_DIR}/external
        BUILD_COMMAND ""
        PATCH_COMMAND ""
        BUILD_IN_SOURCE 1
        SOURCE_DIR ${MCS_ROOT}
        CMAKE_ARGS
        -DCMAKE_POSITION_INDEPENDENT_CODE=ON
        INSTALL_COMMAND ""
        UPDATE_COMMAND ""
        EXCLUDE_FROM_ALL 1)

set(MCS_STATIC_LIBRARIES ${MCS_ROOT}/libmcs.a)

ExternalProject_Add_Step(mcs build_static
        COMMAND make
        DEPENDS mcs
        BYPRODUCTS ${MCS_STATIC_LIBRARIES}
        EXCLUDE_FROM_MAIN 1
        LOG 1
        WORKING_DIRECTORY ${MCS_ROOT}/)

ExternalProject_Add_StepTargets(mcs build_static)
add_library(mcs_static STATIC IMPORTED)
add_dependencies(mcs_static mcs-build_static)
set_target_properties(mcs_static PROPERTIES IMPORTED_LOCATION ${MCS_STATIC_LIBRARIES})
