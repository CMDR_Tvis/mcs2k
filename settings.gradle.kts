rootProject.name = "mcs2k"

pluginManagement {
    val kotlinVersion: String by settings
    plugins { kotlin("jvm") version kotlinVersion }
}
